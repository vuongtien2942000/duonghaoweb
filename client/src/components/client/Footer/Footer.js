import React from 'react'
import '../css/main.css'
import '../css/main.css'
import '../css/linearicons.css'
import '../css/font-awesome.min.css'
import '../css/themify-icons.css'
import '../css/bootstrap.css'
import '../css/owl.carousel.css'
import '../css/nice-select.css'
import '../css/nouislider.min.css'
import '../css/ion.rangeSlider.css'
import '../css/ion.rangeSlider.skinFlat.css'
import '../css/magnific-popup.css'

export default class Footer extends React.Component{
    render(){
        return	<div class="footer ">
        <div class="grid wide ">
            <div class="row ">
                <div class="col l-3 m-6 s-12 ">
                    <h3 class="footer__title ">Menu</h3>
                    <ul class="footer__list ">
                        <li class="footer__item ">
                            <a href="# " class="footer__link ">Make up</a>
                        </li>
                        <li class="footer__item ">
                            <a href="# " class="footer__link ">Skin Care</a>
                        </li>
                        <li class="footer__item ">
                            <a href="# " class="footer__link ">Hair Care</a>
                        </li>
                        <li class="footer__item ">
                            <a href="# " class="footer__link ">Perfume</a>
                        </li>
                        <li class="footer__item ">
                            <a href="# " class="footer__link ">Body care</a>
                        </li>
                    </ul>
                </div>
                <div class="col l-3 m-6 s-12 ">
                    <h3 class="footer__title ">Customer support</h3>
                    <ul class="footer__list ">
                        <li class="footer__item ">
                            <a href="# " class="footer__link ">Shopping guide</a>
                        </li>
                        <li class="footer__item ">
                            <a href="# " class="footer__link ">Troubleshooting</a>
                        </li>
                        <li class="footer__item ">
                            <a href="# " class="footer__link ">Purchase Policy</a>
                        </li>
                        <li class="footer__item ">
                            <a href="# " class="footer__link ">Return Policy</a>
                        </li>
                    </ul>
                </div>
                <div class="col l-3 m-6 s-12 ">
                    <h3 class="footer__title ">Contact</h3>
                    <ul class="footer__list ">
                        <li class="footer__item ">
                            <span class="footer__text ">
                                    <i class="fas fa-map-marked-alt "></i> 72 Nguyễn Trai, Thanh Xuan, Ha Noi
                                </span>
                        </li>
                        <li class="footer__item ">
                            <a href="# " class="footer__link ">
                                <i class="fas fa-phone "></i> 099999999999
                            </a>
                        </li>
                        <li class="footer__item ">
                            <a href="# " class="footer__link ">
                                <i class="fas fa-envelope "></i> duonghao@gmail.com
                            </a>
                        </li>
                        <li class="footer__item ">
                            <div class="social-group ">
                                <a href="# " class="social-item "><i class="fab fa-facebook-f "></i>
                                    </a>
                                <a href="# " class="social-item "><i class="fab fa-twitter "></i>
                                    </a>
                                <a href="# " class="social-item "><i class="fab fa-pinterest-p "></i>
                                    </a>
                                <a href="# " class="social-item "><i class="fab fa-invision "></i>
                                    </a>
                                <a href="# " class="social-item "><i class="fab fa-youtube "></i>  
                                    </a>
                            </div>
                        </li>
                    </ul>
                </div>
                {/* <div class="col l-3 m-6 s-12 ">
                    <h3 class="footer__title ">Đăng kí</h3>
                    <ul class="footer__list ">
                        <li class="footer__item ">
                            <span class="footer__text ">Đăng ký để nhận được được thông tin ưu đãi mới nhất từ chúng tôi.</span>
                        </li>
                        <li class="footer__item ">
                            <div class="send-email ">
                                <input class="send-email__input " type="email " placeholder="Nhập Email... "/>
                                <a href="# " class="send-email__link ">
                                    <i class="fas fa-paper-plane "></i>
                                </a>
                            </div>
                        </li>
                    </ul>
                </div> */}
            </div>
        </div>
        <div class="copyright ">
            <span class="footer__text "> Copyright by <a class="footer__link " href="# "> Duong Hao</a></span>
        </div>
    </div>
// 		<footer className="footer-area section_gap">
// 		<div className="container">
// 			<div className="row">
// 				<div className="col-lg-3  col-md-6 col-sm-6">
// 					<div className="single-footer-widget">
// 						<h6>About Us</h6>
// 						<p>
							
// We are dedicated to you.Love our customers.Shoe lovers with the desire to bring the best quality shoes to you.
// 						</p>
// 					</div>
// 				</div>
// 				<div className="col-lg-4  col-md-6 col-sm-6">
// 					<div className="single-footer-widget">
// 						<h6>Newsletter</h6>
// 						<p>Stay update with our latest</p>
// 						<h6>Mr Tien</h6>
// 						<h6>Ms Nga </h6>
// 						<h6>Ms P.Anh</h6>
// 					</div>
// 				</div>
// 				<div className="col-lg-3  col-md-6 col-sm-6">
// 					<div className="single-footer-widget mail-chimp">
// 						<h6 className="mb-20">Instragram Feed</h6>
// 						<ul className="instafeed d-flex flex-wrap">
// 							<li><img src={require("../img/i1.jpg").default} alt=""/></li>
// 							<li><img src={require("../img/i2.jpg").default} alt=""/></li>
// 							<li><img src={require("../img/i3.jpg").default} alt=""/></li>
// 							<li><img src={require("../img/i4.jpg").default} alt=""/></li>
// 							<li><img src={require("../img/i5.jpg").default} alt=""/></li>
// 							<li><img src={require("../img/i6.jpg").default} alt=""/></li>
// 							<li><img src={require("../img/i7.jpg").default} alt=""/></li>
// 							<li><img src={require("../img/i8.jpg").default} alt=""/></li>
// 						</ul>
// 					</div>
// 				</div>
// 				<div className="col-lg-2 col-md-6 col-sm-6">
// 					<div className="single-footer-widget">
// 						<h6>Follow Us</h6>
// 						<p>Let us be social</p>
// 						<div className="footer-social d-flex align-items-center">
// 							<a href="https://www.facebook.com/profile.php?id=100004454478337"><i className="fa fa-facebook"></i></a>
// 							{/* <a href="#"><i className="fa fa-twitter"></i></a>
// 							<a href="#"><i className="fa fa-dribbble"></i></a>
// 							<a href="#"><i className="fa fa-behance"></i></a> */}
// 						</div>
// 					</div>
// 				</div>
// 			</div>
// 			<div className="footer-bottom d-flex justify-content-center align-items-center flex-wrap">
// 				<p className="footer-text m-0">
// </p>
// 			</div>
// 		</div>
// 	</footer>
	
    }
}